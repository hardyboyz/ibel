<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{
header("HTTP/1.0 404 Not Found");
exit();
}
global $content;
?>
<!-- main start -->
<!-- ================ -->
<div class="main col-md-12">
	<div class="row">
		<h2 class="text-center">Paket Tour</h2>
		<?php $packages = get_packages(32); 
		$c = 0;
		foreach($packages as $pack){
			$c++;
			if($c<=4){
				preg_match_all('/<img[^>]+>/i',$pack['content'], $result); 
				//print_r($result);
				 ?>
				<div class="col-sm-3">
					<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
						<div class="overlay-container">
							<?php echo isset($result[0][0]) ? $result[0][0] : '<img src="'.$config->url.$config->templates.'images/portfolio-1.jpg">' ?>
							<div class="overlay-to-top">
								<p class="small margin-clear"><em><?php echo $pack['content'] ?></em></p>
							</div>
						</div>
						<div class="body">
							<h3><?php echo $pack['title'] ?></h3>
							<div class="separator"></div>
							<!--<p><?php echo substr($pack['content'],0,150) ?></p>-->
							<a href="<?php echo $config->url.$pack['url'] ?>" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
						</div>
					</div>
				</div>
				<?php 
			}
		} 
		?>
	</div>
	
	<div class="row">
		<h2 class="text-center">Itinerary</h2>
		<?php $packages = get_packages(14); 
		foreach($packages as $pack){
		preg_match_all('/<img[^>]+>/i',$pack['content'], $result); 
		//print_r($result);
		 ?>
		<div class="col-sm-3">
			<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
				<div class="overlay-container">
					<?php echo isset($result[0][0]) ? $result[0][0] : '<img src="'.$config->url.$config->templates.'images/portfolio-1.jpg">' ?>
					<div class="overlay-to-top">
						<p class="small margin-clear"><em><?php echo $pack['content'] ?></em></p>
					</div>
				</div>
				<div class="body">
					<h3><?php echo $pack['title'] ?></h3>
					<div class="separator"></div>
					<!--<p><?php echo substr($pack['content'],0,150) ?></p>-->
					<a href="<?php echo $config->url.$pack['url'] ?>" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<!-- main end -->


