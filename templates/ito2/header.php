<?php if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{	
header("HTTP/1.0 404 Not Found");
exit();
}
global $content;

?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8">
<title><?php echo $content['title_browser'] ?></title>
<meta name="description" content="<?php echo $content['description'] ?>. touring and traveling for your vacation with <?php echo $config->sitename ?>">
<meta name="author" content="itobelitungtour">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS -->
<link href="<?php echo $config->url.$config->templates ?>bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Font Awesome CSS -->
<link href="<?php echo $config->url.$config->templates ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- Fontello CSS -->
<link href="<?php echo $config->url.$config->templates ?>fonts/fontello/css/fontello.css" rel="stylesheet">

<!-- Plugins -->
<link href="<?php echo $config->url.$config->templates ?>plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="<?php echo $config->url.$config->templates ?>css/animations.css" rel="stylesheet">
<link href="<?php echo $config->url.$config->templates ?>plugins/hover/hover-min.css" rel="stylesheet">

<!-- the project core CSS file -->
<link href="<?php echo $config->url.$config->templates ?>css/style.css" rel="stylesheet" >

<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
<link href="<?php echo $config->url.$config->templates ?>css/skins/light_blue.css" rel="stylesheet">

<!-- Custom css --> 
<link href="<?php echo $config->url.$config->templates ?>css/custom.css" rel="stylesheet">

<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/modernizr.js"></script>

		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Contact form -->
		<script src="<?php echo $config->url.$config->templates ?>plugins/jquery.validate.js"></script>

		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="<?php echo $config->url.$config->templates ?>js/custom.js"></script>

		<?php if(isset($_SESSION['login'])){ 
		if($_GET['url'] == "logout"){
			logout();
		}
		?>
		<script>
		function logout(){
			x=confirm('Are you sure want to logout?');
			if(x==true){
				window.location='logout';
			}
		}
		</script>
		<?php } ?>

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7503857010695987",
          enable_page_level_ads: true
     });
</script>
</head>

<!-- body classes:  -->
<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
<body class="no-trans  ">

<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">

<!-- header-container start -->
<div class="header-container">

<!-- header-top start -->
<!-- classes:  -->
<!-- "dark": dark version of header top e.g. class="header-top dark" -->
<!-- "colored": colored version of header top e.g. class="header-top colored" -->
<!-- ================ -->
<div class="header-top dark ">
<div class="container">
<div class="row">
	<div class="col-xs-3 col-sm-6 col-md-9">
		<!-- header-top-first start -->
		<!-- ================ -->
		<div class="header-top-first clearfix">
			<ul class="social-links circle small clearfix hidden-xs">
				<li class="facebook"><a target="_blank" href="http://www.facebook.com/ito.belitung"><i class="fa fa-facebook"></i></a></li>
				<li class="googleplus"><a target="_blank" href="https://plus.google.com/106614834223195296824"><i class="fa fa-google-plus"></i></a></li>
			</ul>
			<div class="social-links hidden-lg hidden-md hidden-sm circle small">
				<div class="btn-group dropdown">
					<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
					<ul class="dropdown-menu dropdown-animation">
						<li class="facebook"><a target="_blank" href="http://www.facebook.comitobelitung"><i class="fa fa-facebook"></i></a></li>
						<li class="googleplus"><a target="_blank" href="https://plus.google.com/106614834223195296824"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
			<ul class="list-inline hidden-sm hidden-xs">
				<li><i class="fa fa-map-marker pr-5 pl-10"></i>JL. Lettu Mad Daud No.15 Tanjung Pandan Belitung 33415</li>
				<li><i class="fa fa-phone pr-5 pl-10"></i>0812 7386 3003</li>
				<li><i class="fa fa-smile-o pr-5 pl-10"></i>54C71BEA</li>
				<li><i class="fa fa-envelope-o pr-5 pl-10"></i> itobelitungtour@gmail.com</li>
			</ul>
		</div>
		<!-- header-top-first end -->
	</div>
	</div>
</div>
</div>
</div>
<!-- header-top end -->

<!-- header start -->
<!-- classes:  -->
<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
<!-- "full-width": mandatory class for the full-width menu layout -->
<!-- "centered": mandatory class for the centered logo layout -->
<!-- ================ --> 
<header class="header  fixed   clearfix">

<div class="container">
<div class="row">
	<div class="col-md-3">
		<!-- header-left start -->
		<!-- ================ -->
		<div class="header-left clearfix">

			<!-- logo -->
			<div id="logo" class="logo">
				<a href="<?php echo $config->url ?>"><img id="logo_img" src="<?php echo $config->url ?>images/ito_belitung_tour.png" alt="<?php echo $config->sitename ?>"></a>
			</div>

			<!-- name-and-slogan -->
			<div class="site-slogan center">
				Teman perjalanan wisata terbaik di pulau belitung
			</div>
			
		</div>
		<!-- header-left end -->

	</div>
	<div class="col-md-9">

		<!-- header-right start -->
		<!-- ================ -->
		<div class="header-right clearfix">
			
		<!-- main-navigation start -->
		<!-- classes: -->
		<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
		<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
		<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
		<!-- ================ -->
		<div class="main-navigation  animated with-dropdown-buttons">

			<!-- navbar start -->
			<!-- ================ -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">

					<!-- Toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-collapse-1">
						<!-- main-menu -->
						      <ul class="nav navbar-nav">
						         <?php echo buildMenu(0); ?>
						      </ul>
					</div>

				</div>
			</nav>
			<!-- navbar end -->

		</div>
		<!-- main-navigation end -->	
		</div>
		<!-- header-right end -->

	</div>
</div>
</div>

</header>
<!-- header end -->
</div>
<!-- header-container end -->
