<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{
	header("HTTP/1.0 404 Not Found");
	exit();
}
global $content;
?>
<!-- main-container start -->
<!-- ================ -->
<section class="main-container">
<div class="container">
	<div class="row">
<?php
if(isset($content['file'])){
	if($content['file'] != ""){
		if(file_exists($config->templates.$content['content'])){
			include ($config->templates.$content['file']);
		}else{
			//echo "<br>Module ".$content['file']. " not available";
			include ($config->templates.'home.php');
		}
	}
}else{
	echo '<h2>'.$content['title'].'</h2>';
	echo '<p>'.$content['content'].'</p>';
}
?>
	</div>
</div>
</section>
<!-- main-container end -->	
