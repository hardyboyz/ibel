<?php
session_start();
session_regenerate_id();
define( '_HARDYBOYZ_FRAMEWORK_','If you didnt define this, then your files are not secure.');

require_once ("./config/config.php");

$config = new Config();
$default_url = "home";
if($_GET['url'] != "") {
	$default_url = $_GET['url'];
}
//if(strpos($_GET['url'],"api") > 0) $json = true;

require_once ($config->functions);

require_once ($config->classes."db.php");

$db = new MysqliDb($config->db['host'], $config->db['username'],$config->db['password'], $config->db['dbname'] );

require ($config->templates.'index.php');

exit();

?>
