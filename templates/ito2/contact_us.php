<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{	
	header("HTTP/1.0 404 Not Found");
	exit();
}

if(isset($_GET['contact_us'])){
	if(strlen($_POST['name']) > 0 && strlen($_POST['phone']) > 0 && strlen($_POST['email']) > 0 && strlen($_POST['message']) > 0 ){
		send_email($_POST, "contact_us");
		//echo ":::".$_POST;
	}
}

require_once  $config->classes.'recaptcha/autoload.php';
$lang = 'en';
if(isset($_POST['g-recaptcha-response'])):
    $recaptcha = new \ReCaptcha\ReCaptcha($config->secret);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

    if ($resp->isSuccess()):
        ?>
        :::Thank you for contacting us. we will reply your message soon.
      HDR .. <?php echo $config->sitename ?>:::
        <?php
    else:
        ?>
        :::Something went wrong:::
    <?php
    endif;
else:
?>

<form class="form-horizontal" id="contact_us" method="post">

    <div class="form-group">
        <div class="col-xs-6 input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            <input type="text" class="form-control" id="inputEmail" name="name" required placeholder="Nama">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
            <input type="text" class="form-control" id="phone" name="phone" required placeholder="No. Telepon">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
            <textarea class="form-control" id="message" name="message" required placeholder="Message" rows="4"></textarea>
        </div>
    </div>
     <div class="g-recaptcha" data-sitekey="<?php echo $config->siteKey; ?>"></div>
            <script type="text/javascript"
                    src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
            </script>
    <div class="form-group">
        <div class="buttonreg">
            <button type="submit" class="btn btn-primary">Send Message</button>
        </div>
    </div>
</form>

<script>
$(document).ready(function(){
	$("form[id=contact_us]").submit(function() {
		$.ajax({
			type: "POST",
			url: "?contact_us=true",
			data: $('form[id=contact_us]').serialize(),
			success: function(info){
				display	= info.split(":::");
				alert(display[2]);
				window.location=display[1];
			}			
		});
	return false;
	});
});
</script>
<?php endif; ?>
</div>
