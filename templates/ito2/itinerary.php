<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{
	header("HTTP/1.0 404 Not Found");
	exit();
}
?>
<link href="<?php echo $config->url.$config->js_extension ?>jquery.dataTables.css" rel="stylesheet">
<script src="<?php echo $config->url.$config->js_extension ?>jquery.dataTables.min.js"></script>
<script src="<?php echo $config->url.$config->js_extension ?>tinymce/tinymce.min.js"></script>
<script> 
	$(function() {   
	  tinyMCE.init({selector: '#content', 
						plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools"
    ],
					  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons", file_browser_callback: RoxyFileBrowser}); 
	});
	function RoxyFileBrowser(field_name, url, type, win) {
	  var roxyFileman = '<?php echo $config->url.$config->plugins ?>fileman/index.html';
	  if (roxyFileman.indexOf("?") < 0) {     
		roxyFileman += "?type=" + type;   
	  }
	  else {
		roxyFileman += "&type=" + type;
	  }
	  roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
	  if(tinyMCE.activeEditor.settings.language){
		roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
	  }
	  tinyMCE.activeEditor.windowManager.open({
		 file: roxyFileman,
		 title: 'Roxy Fileman',
		 width: 850, 
		 height: 650,
		 resizable: "yes",
		 plugins: "media",
		 inline: "yes",
		 close_previous: "no"  
	  }, {     window: win,     input: field_name    });
	  return false; 
	}
</script>
<script>
		$(document).ready(function() {
			$('#reservation_list').dataTable();
		} );
</script>
<?php
if(isset($_GET['action'])){
		$table = "contents";
		$action = $_GET['action'];
		if($action ===  "update" ) {
			$params = array();
			$params = $_POST;
			update($table, "id",$params['id'],$params);
		}
		
		else if($action ===  "delete" ) {
			if(delete($table, $_GET['id'],"id")){
				echo "<script>window.location='data-paket'</script>";
			}
			
		}
		
		else if($action ===  "edit" ) {
			package_edit($_GET['id']);
		}
		
		else if($action ===  "delete" ) {
			delete($_GET['id']);
		}
		
}else{
?>
<table id="reservation_list" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Nama Paket</th>
			<th>Status</th>
			<th>_</th>
		</tr>
	</thead>
	<tbody>
<?php $package = get_packages(14) ;
			$i = 0;
			foreach ($package as $res){
				$i++;
				if($res['visitor'] == 0){
					$status = "Draft";
				}
				if($res['visitor'] == 1){
					$status = "Published";
				}
			
				echo "<tr>
							<td>".$res['title']."</td>
							<td>".$status."</td>
					<td>
					<a href='?action=edit&id=".$res['id']."'><img src=".$config->templates."images/edit.png width='20px'></a> 
					</td>
				</tr>";
			}
?>
</tbody>
</table>
<?php
}
?>
</div>
<script>
		
	$(document).ready(function(){
		$("form#package_form").submit(function() {	
			$('#submit').attr('disabled','disabled');
			$('#submit').html('Updating Your Tour Package. Please wait...');
			tinyMCE.triggerSave();
				$.ajax({
					type: "POST",
					url: "?action=update",
					data: $('form[id=package_form]').serialize(),
					success: function(info){
						//alert(info);
						data = info.split(":::");
						//console.log(info);
						$('#message').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-ok"></span> Your content has been Updated.</strong></div>');
						
						setTimeout(function(){
							window.location.reload();
						}, 3000);
						//$('#add').removeAttr('disabled');
						//$('#add').html('Update');
					},
					
				});
			return false;
		});
	});
	</script>
	</div>

	<?php get_footer(); ?>
