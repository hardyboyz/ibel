<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{	
	header("HTTP/1.0 404 Not Found");
	exit();
}

//print_r($config);

function get_header($json = null){
	global $config;
	if($json == null) include $config->templates."header.php";
}

function get_footer($json = null){
	global $config;
	if($json == null) include $config->templates."footer.php";
}

function get_sidebar(){
	global $config;
	include $config->templates."sidebar.php";
}

function get_content(){
	global $config;
	include $config->templates."content_default.php";
}

function login_group(){
	$login = "visitor";
	if(isset($_SESSION['login']) && is_array($_SESSION['login'])){
		if($_SESSION['login'][0]['groups']==1){
			$login = 'admin';
		}
		if($_SESSION['login'][0]['groups']==2){
			$login = 'user';
		}
		if($_SESSION['login'][0]['groups']==3){
			$login = 'finance';
		}
	}
	return $login;
}

function get_gallery(){
	global $config;
	if(isset($_SESSION['login'])){
	?>
	<iframe src="<?php echo $config->url.$config->plugins ?>gallery/index.php" style="width:100%;height:400px;border:none;overflow:none"></iframe>
	<?php
	}
}

function insert($table,$dataToInsert){
	global $db;
	$id = $db->insert($table, $dataToInsert);
	if($id){
		echo ':::Thank you for your '. $table;
	}else{
		echo ':::'.$table.'  Error : '.$db->getLastQuery().'--'.$db->getLastError();
	}
}

function insert_reservation($table,$dataToInsert){
	global $db;
	$travel_date = date('Y-m-d',strtotime($dataToInsert['travel_date']));
	$dataToInsert['travel_date'] = $travel_date;
	$id = $db->insert($table, $dataToInsert);
	if($id){
		echo ':::Insert data into '.$table.' has been succeeded.';
		send_email($dataToInsert);
	}else{
		echo ':::Insert data Error ::: '.$db->getLastError();
	}
}

function insert_sukasuka($table,$dataToInsert){
	global $db;
	$travel_date = date('Y-m-d',strtotime($dataToInsert['travel_date']));
	$dataToInsert['travel_date'] = $travel_date;
	//$id = $db->insert($table, $dataToInsert);
	if($db->insert($table, $dataToInsert)){
		echo ':::Insert data into '.$table.' has been succeeded.';
		send_email($dataToInsert);
	}else{
		echo ':::Insert data Error ::: '.$db->getLastError();
	}
}

function get_content2($url){
	global $db;
	$db->where ("url", $url);
	$db->where (login_group(), 1);
	$content 	= $db->getOne ("contents");
	if(count($content) > 0){
		return $content;
	}else{
		$content = array('file'=>'', 'content'=>'','title'=>'');
	}
}

function get_menu_top(){
	global $db, $config;
	$db->where ('category', 1);
	if(get_menu_admin()=='admin'){
		$db->where ('admin', 1);
		//$db->Where ('isLogin', 1);
	}
	elseif(get_menu_admin()=='user'){
		$db->where ('user', 1);
	}
	elseif(get_menu_admin()=='finance'){
		$db->where ('finance', 1);
	}else{
		$db->where ('visitor', 1);
	}
	$db->orderBy("position","asc");
	$menu = $db->get('contents');
	for($i=0;$i<=sizeof($menu)-1;$i++){
		if($menu[$i]['url']=='home'){
			$home = '<i class="fa fa-home"></i>';
		}else{
			$home = '';
		}
		if($menu[$i]['url']==$_GET['url']){
			echo "<li class='selected'><a href=".$config->url.$menu[$i]['url'].">".$home.$menu[$i]['title']."</a></li>";
		}elseif($menu[$i]['url']=="logout()"){
			echo "<li><a onclick=".$menu[$i]['url']." href='#'>".$menu[$i]['title']."</a></li>";
		}
		else{
			echo "<li><a href=".$config->url.$menu[$i]['url'].">".$home.$menu[$i]['title']."</a></li>";
		}
	}
}

function get_menu_admin(){
	$login = "visitor";
	if(isset($_SESSION['login'])){
		if($_SESSION['login'][0]['groups']==1){
			$login = 'admin';
		}
		if($_SESSION['login'][0]['groups']==2){
			$login = 'user';
		}
		if($_SESSION['login'][0]['groups']==3){
			$login = 'finance';
		}
	}
	return $login;
}

function get_menu($id=null, $parent=null, $category=null){
	global $db, $config;
	
	$cols = array('id','title','content','url', 'file');
	
	if(isset($id)){
		$db->where ('id', $id);
	}
	
	if(isset($parent)){
		$db->where ('parent', $parent);
	}
	
	if(isset($category)){
		$db->where ('category', $category);
	}
	
	$db->orderby('position', 'asc');
	
	
	$menu = $db->get('contents',null,$cols);
	return $menu;
}

function get_menu_right(){
	global $db, $config;
	$db->where ('category', 2);
	if(get_menu_admin()==1){
		$db->where ('admin', 1);
	}
	$menu = $db->get('contents');
	for($i=0;$i<=sizeof($menu)-1;$i++){
		if($menu[$i]['url']==$_GET['url']){
			echo "<li class='subselected'><a href=".$config->url.$menu[$i]['url'].">".$menu[$i]['title']."</a></li>";
		}else{
			echo "<li><a href=".$config->url.$menu[$i]['url'].">".$menu[$i]['title']."</a></li>";
		}
	}
}

function get_testi($status=null){
	global $db;
		if($status != "all"){
		$db->where ('status', 1);
	}
	$db->orderBy ('id', 'DESC');
	$reservations = $db->get('testimonials');
	return $reservations;
}

function get_reservations(){
	global $db, $config;
	$db->join("contents B", "A.id_package=B.id", "LEFT");
	$db->orderBy ('travel_date', 'ASC');
	$reservations = $db->get('reservations A');
	return $reservations;
}

function get_packages($id,$otherid=null){
	global $db;
	$db->where("parent",$id);
	if(is_array($otherid)){
		foreach($otherid as $o){
			$db->orwhere("parent",$o);
		}
	}
	$db->orderBy ('position', 'ASC');
	$packages = $db->get('contents');
	return $packages;
}

function print_invoice($id){
	global $db, $config;
	$db->join("contents B", "A.id_package=B.id", "LEFT");
	$db->orderBy ('travel_date', 'ASC');
	$db->where("A.id_reservation",$id);
	$reservations = $db->getOne('reservations A');
	print_r ($reservations);
}

function update($table,$field,$id,$params){
	global $db;
	$db->where ($field, $id);
	$update = $db->update ($table, $params);
	if($update){
		echo ":::Update has been successfully.";
	}else{
		echo ":::".$db->getLastError();
	}
}

// Menu builder function, parentId 0 is the root
function buildMenu($parent)
{
	global $db;
	// Select all entries from the menu table
	//$result=mysql_query("SELECT id, label, link, parent FROM menu ORDER BY parent, sort, label");
	
	$db->orderBy('position','asc');
	$db->where('category',1);
	$db->where (login_group(), 1);
	$cols = array('id','parent','title','url');
	$result = $db->get('contents',null,$cols);
	// Create a multidimensional array to conatin a list of items and parents
	$menu = array(
		'items' => array(),
		'parents' => array()
	);
	// Builds the array lists with data from the menu table
	foreach ($result as $items)
	{
		$menu['items'][$items['id']] = $items;
		$menu['parents'][$items['parent']][] = $items['id'];
	}

	
   $html = "";
   if (isset($menu['parents'][$parent]))
   {
	  
       foreach ($menu['parents'][$parent] as $itemId)
       {
		  $class="";
		  if($_GET['url'] == $menu['items'][$itemId]['url']){
			$class="active";  
		  }
          if(!isset($menu['parents'][$itemId]))
          {
             $html .= "<li class='dropdown $class'> <a href='".$menu['items'][$itemId]['url']."'>".$menu['items'][$itemId]['title']."</a></li>";
          }
          if(isset($menu['parents'][$itemId]))
          {
			  $icon = " <b class='caret'></b></a>";
			  if($menu['items'][$itemId]['parent'] != 0){
				  $icon = " &nbsp;<b class='caret-right'></b></a>";
			  }
			  $html .= "
             <li class='dropdown'> <a data-toggle='dropdown' class='dropdown-toggle' href='".$menu['items'][$itemId]['url']."'>".$menu['items'][$itemId]['title'].$icon;
			 $html .= "<ul class='dropdown-menu sub-menu'>\n";
             
             $html .= buildMenu($itemId, $menu);
             $html .= "</li> \n";
          }
       }
       $html .= "</ul> \n";
   }
   return $html;
}

function is_home(){
	return $_GET['url']=="home" ?  true : false;
}


function get_month_list($sameMonth=null){
	for($i=1;$i<=12;$i++){
		$monthName[$i] = date('F', mktime(0, 0, 0, $i, 10));
		$m = date('m', mktime(0, 0, 0, $i, 10));
		if($sameMonth==$m){
			echo "<option value=$m selected>$monthName[$i]</option>";
		}else{
			echo "<option value=$m>$monthName[$i]</option>";
		}
	}
}
  
function get_year_list($year=null){
	for($i=2012;$i<=date('Y')+1;$i++){
		if($year==$i){
			echo "<option value=$i selected>$i</option>";
		}else{
			echo "<option value=$i>$i</option>";
		}
	}
}

function get_list_uploaded_files(){
	global $db;
	$lists = $db->get("download");
	return $lists;
}

function remove_uploaded_files($id,$filename){
	global $db;
	$db->where("id",$id);
	if($db->delete("download")){
		@unlink("includes/uploads/".$filename);
		echo "<script>alert('File has been deleted');self.history.back();</script>";
	}
}

function cek_login($username,$password,$json){
	global $db;
	if(strlen($username) > 0 && strlen($password) > 0){
		$username	= strtolower($username);
		$password	= md5($password);
		$db->where('username',$username);
		$db->where('password',$password);
		$results = $db->get ('users');
		if(sizeof($results) > 0){
			$_SESSION['login']	= $results;
			if ($_SESSION['login'][0]['groups'] == 1){
					echo ":::reservasi";
				}
				if($_SESSION['login'][0]['groups'] == 2){
					echo ":::personal-details";
				}
				if($_SESSION['login'][0]['groups'] == 3){
					echo ":::application-list";
				}

		}else{
			echo ":::Login Error.:::";
		}
	}
	exit;
}

if(isset($_GET['logout'])){
	logout();
}

function logout(){
	global $config;
	if(isset($_SESSION['login'])){
		unset($_SESSION['login']);
		session_destroy();
	}
	echo "<script>window.location='".$config->url."';</script>";
}

function delete($table,$id,$field=null){
	global $db;
	if($field==null) $field = "id";
	$db->where($field,$id);
	if($db->delete($table)) return true;
}

function upload_gallery($json = null){
	global $config;
	if($json == true){
		header("location:".$config->url.$config->plugins."gallery/server/php");
	}
}

function send_email($params, $from=null){
	global $db;
	
	if($from == "contact_us"){
		$to		= $config->email;
		$subject 	= "Contact Us Website Form";
		$message = "
				<html>
				<head>
				<title>Contact Us Website Form</title>
				</head>
				<body>
				<p>Nama : ".$params['name']."</p>
				<p>No. Telepon : ".$params['phone']."</p>
				<p>Email : ".$params['email']."</p>
				<p>Message : ".$params['message']."</p>
				</body>
				</html>
				";
	}else{
		$db->where("id",$params['id_package']);
		$package = $db->getOne("contents");
		//$to 		= $params['email'];
		$to 		= "admin@itobelitungtour.com";
		$subject 	= "Reservation";
		
	$message = "
				<html>
				<head>
				<title>Reservation</title>
				</head>
				<body>
				<p>Paket Pilihan : ".$package['title']."</p>
				<p>Tanggal Ketibaan : ".$params['travel_date']."</p>
				<p>Total Pax : ".$params['total_pax']."</p>
				<p>Nama : ".$params['name']."</p>
				<p>Alamat : ".$params['address']."</p>
				<p>No. Telepon : ".$params['phone']."</p>
				<p>Email : ".$params['email']."</p>
				<p>Keterangan Tambahan : ".$params['remarks']."</p>
				<p>=================================</p>
				<p>Mobil : ".$params['mobil']."</p>
				<p>Hotel : ".$params['hotel']."</p>
				<p>Snorkeling : ".$params['snorkeling']."</p>
				<p>Boat : ".$params['boat']."</p>
				</body>
				</html>
				";
			}
	write_log($message);
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: <'.$config->email.'>' . "\r\n";
	$headers .= 'Cc: itobelitungtour@gmail.com' . "\r\n";
	$headers .= 'Bcc: hardi84@gmail.com' . "\r\n";
	mail($to,$subject,$message,$headers);
	echo ":::contact-us";
}

function write_log($message, $logfile='') {
  $myfile = fopen("error.log", "a") or die("Unable to open file!");
	fwrite($myfile, $message);
	fclose($myfile);
}

function reservasi_edit($id){
	global $db;
	$db->where('id_reservation', $id);
	$reserv = $db->getOne('reservations');
	//print_r($reserv);
	?>
		<div class="col-lg-12">
		<div id="message"></div>
</div>
<div id="reservation_edit">
						<form role="form" method="post" id="reservation_form">
							<input type="hidden" name="id_reservation" value="<?= $reserv['id_reservation'] ?>">
							<div class="col-lg-12">
								<div id="message"></div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="package">Pilihan Paket</label>
									<div class="input-group">
										<select class="form-control" id="package" name="id_package" readonly>
											<?php $menu = get_menu(null,'14'); 
													foreach($menu as $q){
														if($q['id'] == $reserv['id_package']){
															echo "<option value='".$q['id']."' selected>".$q['title']."</option>";
														}else{
															echo "<option value='".$q['id']."'>".$q['title']."</option>";
														}
												}
												?>
										</select>
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>	
								</div>
								<div class="form-group">
									<label for="total_pax">Total Pax</label>
									<div class="input-group">
										<input type="text" class="form-control" name="total_pax" id="total_pax" required value="<?= $reserv['total_pax'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								<div class="form-group">
									<label for="jml_hari">Jumlah Hari</label>
									<div class="input-group">
										<input type="text" class="form-control" name="jml_hari" id="jml_hari" required value="<?= $reserv['jml_hari'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								
								<div class="form-group">
									<label for="mobil">Mobil</label>
									<div class="input-group">
										<input type="text" class="form-control" name="mobil" id="mobil"  value="<?= $reserv['mobil'] ?>">
										<span class="input-group-addon"></span>
									</div>
								</div>

							<div class="form-group">
									<label for="hotel">Hotel</label>
									<div class="input-group">
										<input type="text" class="form-control" name="hotel" id="hotel"  value="<?= $reserv['hotel'] ?>">
										<span class="input-group-addon"></span>
									</div>
								</div>
	
							<div class="form-group">
									<label for="snorkeling">Snorkeling + Pelampung</label>
									<div class="input-group">
										<input type="text" class="form-control" name="snorkeling" id="snorkeling"  value="<?= $reserv['snorkeling'] ?>">
										<span class="input-group-addon"></span>
									</div>
								</div>

							<div class="form-group">
									<label for="boat">Boat ke Pulau</label>
									<div class="input-group">
										<input type="text" class="form-control" name="boat" id="boat"  value="<?= $reserv['boat'] ?>">
										<span class="input-group-addon"></span>
									</div>
								</div>
								<div class="form-group">
									<label for="status">Status</label>
									<div class="input-group">
										<?php $status = array('0'=>'Pending','1'=>'OK','2'=>'Cancel');
										?>
										<select name="status" class="form-control">
										<?php
										foreach($status as $q => $val){
											if($q == $reserv['status']){
												echo "<option value=".$q." selected>".$val."</option>";
											}else{
												echo "<option value=".$q." >".$val."</option>";
											}
										}
										?>
										</select>
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
							</div>
							
							<div class="col-lg-8">	
								<div class="form-group">
									<label for="travel_date">Tanggal Keberangkatan</label>
									<div class="input-group">
										<input type="text" class="form-control" name="travel_date" id="travel_date" required value="<?= $reserv['travel_date'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								
								<div class="form-group">
									<label for="InputName">Nama</label>
									<div class="input-group">
										<input type="text" class="form-control" name="name" id="name" required value="<?= $reserv['name'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								<div class="form-group">
									<label for="address">Alamat</label>
									<div class="input-group">
										<textarea name="address" id="address" class="form-control" rows="2" required><?= $reserv['address'] ?></textarea>
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								<div class="form-group">
									<label for="phone">No.Telpon</label>
									<div class="input-group">
										<input type="text" class="form-control" name="phone" id="phone"  required value="<?= $reserv['phone'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								<div class="form-group">
									<label for="InputEmail"> Email</label>
									<div class="input-group">
										<input type="email" class="form-control" id="email" name="email"  required value="<?= $reserv['email'] ?>">
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								<div class="form-group">
									<label for="remarks">Keterangan Tambahan</label>
									<div class="input-group">
										<textarea name="remarks" id="remarks" class="form-control" rows="2" required><?= $reserv['remarks'] ?></textarea>
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								
								<div class="form-group">
									<label for="total_amount">Total Harga</label>
									<div class="input-group">
										<input type="text" name="total_amount" id="total_amount" class="form-control"  value="<?= $reserv['total_amount'] ?>"></textarea>
										<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
									</div>
								</div>
								
						<hr />
							<button type="submit" name="submit" id="submit" class="btn btn-primary pull-right col-xs-4">UPDATE</button>
							<button type="button" name="cancel" id="cancel" class="btn btn-default" onclick="self.history.back()">CANCEL</button>
							</div>
						</form>
		
	</div>
</form>
	
	<?php
}

function package_edit($id){
	global $db;
	$db->where('id', $id);
	$reserv = $db->getOne('contents');
	//print_r($reserv);
	//$contents = explode(":::",$reserv['content']);
	?>
		<div class="col-lg-12">
		<div id="message"></div>
</div>
<div id="package_edit">
		<form role="form" method="post" id="package_form" novalidate>
			<input type="hidden" name="id" value="<?= $reserv['id'] ?>">
			<div class="col-lg-12">
				<div id="message"></div>
			</div>
			<div class="form-group">
					<label for="title">Title</label>
					<div class="input-group">
						<input type="text" class="form-control" name="title" id="title" required value="<?= $reserv['title'] ?>">
						<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
				</div>
				<div class="form-group">
					<label for="content">Content</label>
					<div class="input-group">
						<textarea name="content" id="content" class="form-control" rows="15" required><?= $reserv['content']  ?></textarea>
						<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
				</div>
				<div class="form-group col-lg-4">
					<label for="visitor">Status</label>
					<div class="input-group">
						<?php $status = array('0'=>'Draft','1'=>'Published');
						?>
						<select name="visitor" class="form-control">
						<?php
						foreach($status as $q => $val){
							if($q == $reserv['visitor']){
								echo "<option value=".$q." selected>".$val."</option>";
							}else{
								echo "<option value=".$q." >".$val."</option>";
							}
						}
						?>
						</select>
						<span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
				</div>
			</div>
				
		<hr />
			<button type="submit" name="submit" id="submit" class="btn btn-primary col-xs-4">UPDATE</button>
			<button type="button" name="cancel" id="cancel" class="btn btn-default col-xs-4" onclick="self.history.back()">CANCEL</button>
			</div>
		</form>
		
</div>
	
	<?php
}

?>
