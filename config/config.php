<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{
	header("HTTP/1.0 404 Not Found");
	exit();
}

class Config {
	public $url, $sitename, $includes, $templates, $classes,$js_extension,$functions,$plugins,$uploads;

// Register API keys at https://www.google.com/recaptcha/admin
	public $siteKey = '6LdzxxMTAAAAACy09NUJIiukTY40fYtiluFIHG5m';
	public $secret = '6LdzxxMTAAAAALqtoxPmgcEwMimzpxghQqV5jtl5';
	
	function __construct(){
		$this->url 		= 'http://itobelitungtour.com/';
		$this->email 	= 'itobelitungtour@gmail.com';
		$this->sitename	= "Website Ito Belitung Tour";
		$this->includes	= "includes/";
		$this->templates	= "templates/ito2/";
		$this->classes	= "class/";
		$this->js_extension= "js_extension/";
		$this->functions	= "functions/function.php";
		$this->plugins	= "plugins/";
		$this->uploads	= "uploads/";
	}
					
	var $db = array(	'host'	=> "localhost",
				'username'	=>"itobelit_user",
				'password'	=> "P@ssw0rd",
				'dbname'	=> "itobelit_db"
			);
}
?>
