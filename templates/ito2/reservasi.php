<?php
if( !defined( "_HARDYBOYZ_FRAMEWORK_" ) )
{
	header("HTTP/1.0 404 Not Found");
	exit();
}
?>
<link href="<?php echo $config->url.$config->js_extension ?>jquery.dataTables.css" rel="stylesheet">
<script src="<?php echo $config->url.$config->js_extension ?>jquery.dataTables.min.js"></script>
<script>
		$(document).ready(function() {
			$('#reservation_list').dataTable();
		} );
</script>
<?php
if(isset($_GET['action'])){
		$table = "reservations";
		$action = $_GET['action'];
		if($action ===  "update" ) {
			$params = array();
			$params = $_POST;
			update($table, "id_reservation",$params['id_reservation'],$params );
		}
		
		else if($action ===  "delete" ) {
			if(delete($table, $_GET['id'],"id_reservation")){
				echo "<script>window.location='reservasi'</script>";
			}
			
		}
		
		else if($action ===  "edit" ) {
			reservasi_edit($_GET['id']);
		}
		
		else if($action ===  "delete" ) {
			delete($_GET['id']);
		}
		
		else if($action ===  "invoice" ) {
			print_invoice($_GET['id']);
		}
		
}else{
?>
<table id="reservation_list" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>No.</th>
			<th>Tanggal</th>
			<th>Paket</th>
			<th>Nama</th>
			<th>Pax</th>
			<th>Jumlah Hari</th>
			<th>Alamat</th>
			<th>No. Telepon</th>
			<th>Email</th>
			<th>Harga</th>
			<th>Status</th>
			<th>_</th>
		</tr>
	</thead>
	<tbody>
<?php $reserv = get_reservations() ;
			$i = 0;
			foreach ($reserv as $res){
				$i++;
				if($res['status'] == 0){
					$status = "Pending";
				}
				if($res['status'] == 1){
					$status = "OK";
				}
				if($res['status'] == 2){
					$status = "Cancel";
				}
				echo "<tr>
							<td>".$i."</td>
							<td>".date('d-M-Y',strtotime($res['travel_date']))."</td>
							<td>".$res['title']."</td>
							<td>".$res['name']."</td>
							<td>".$res['total_pax']."</td>
							<td>".$res['jml_hari']."</td>
							<td>".$res['address']."</td>
							<td>".$res['phone']."</td>
							<td>".$res['email']."</td>
							<td>".number_format($res['total_amount'])."</td>";
					echo"<td>".$status."</td>
					<td>
					<a href='?action=edit&id=".$res['id_reservation']."'><img src=".$config->templates."images/edit.png width='20px'></a> 
					<a href='#' onclick='reservation_delete(".$res['id_reservation'].")'><img src=".$config->templates."images/delete.png width='20px'></a>
					<a href='#' onclick='invoice(".$res['id_reservation'].")'><img src=".$config->templates."images/invoice.png width='20px'></a>
					</td>
				</tr>";
			}
?>
</tbody>
</table>
<?php
}
?>
</div>
<script>
	function reservation_delete(id){
			x= confirm('Hapus data reservasi ini?');
			if(x==true){
				window.location='?action=delete&id='+id;
			}
	}
	
	function invoice(id){
				window.open('<?php echo $config->url ?>reservasi?action=invoice&id='+id);
	}
	
	$(document).ready(function(){
		$("form#reservation_form").submit(function() {	
			$('#submit').attr('disabled','disabled');
			$('#submit').html('Updating Reservation. Please wait...');
				$.ajax({
					type: "POST",
					url: "?action=update",
					data: $('form[id=reservation_form]').serialize(),
					success: function(info){
						//alert(info);
						data = info.split(":::");
						console.log(info);
						$('#message').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-ok"></span> Reservation has been Updated.</strong></div>');
						
						setTimeout(function(){
							window.location.reload();
						}, 5000);
						//$('#add').removeAttr('disabled');
						//$('#add').html('Update');
					},
					
				});
			return false;
		});
	});
	</script>
	</div>

	<?php get_footer(); ?>
